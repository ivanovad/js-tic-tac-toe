const CROSS = "X";
const ZERO = "O";
const EMPTY = " ";
const playerElement = CROSS;
let fieldContainer;
let numberOfMoves;
let isWinner;
let winCell;
let winCellNumber;
let winOrientation;
let fieldSize;

startGame();

function startGame() {
  let fieldSizeInput = prompt("Ведите размер игрового поля", 3);
  fieldSize = Number(fieldSizeInput);

  while (isNotCorrectInput()) {
    alert("Введите число больше или равное 3");
    fieldSizeInput = prompt("Ведите размер игрового поля", 3);
    fieldSize = Number(fieldSizeInput);
  }

  renderGrid(fieldSize);
  resetParams();
}

function isNotCorrectInput() {
  if (fieldSize < 3 || isNaN(fieldSize)) {
    return true;
  } else {
    return false;
  }
}

function resetParams() {
  fillFieldEmpty();
  numberOfMoves = 0;
  isWinner = false;
  winCellNumber = 0;
  winCell = "";
  winOrientation = "";
  showMessage("");
}

function fillFieldEmpty() {
  fieldContainer = [];
  for (let i = 0; i < fieldSize; i++) {
    fieldContainer[i] = [];
    for (let j = 0; j < fieldSize; j++) {
      fieldContainer[i][j] = EMPTY;
    }
  }
}

/* обработчик нажатия на клетку */
function cellClickHandler(row, col) {
  if (canWriteElemToCell(row, col)) {
    const element = playerElement;
    let elementColor = "black";

    makeMove(row, col, element, elementColor);
    findWinner();

    if (!isEndGame()) {
      makeRandomMove();
      findWinner();
    }
  }
}

function canWriteElemToCell(row, col) {
  if (isEmptyCell(row, col) && !isEndGame()) {
    return true;
  } else {
    return false;
  }
}

function isEndGame() {
  if (isDraw() || isFindWinner()) {
    return true;
  } else {
    return false;
  }
}

function isEmptyCell(row, col) {
  return fieldContainer[row][col] === EMPTY ? true : false;
}

function isDraw() {
  return numberOfMoves === fieldSize ** 2 ? true : false;
}

function isFindWinner() {
  return isWinner;
}

function makeMove(row, col, element, elementColor) {
  writeElemToCell(row, col, element);
  renderSymbolInCell(element, row, col, elementColor);
  increaseMoves();
}

function increaseMoves() {
  numberOfMoves++;
}

function writeElemToCell(row, col, element) {
  fieldContainer[row][col] = element;
}

function makeRandomMove() {
  const { row, col } = generateRowAndCol();
  console.log(row, col);
  const element = getNotPlayerElement();
  const elementColor = "purple";

  makeMove(row, col, element, elementColor);
}

function getNotPlayerElement() {
  return playerElement === CROSS ? ZERO : CROSS;
}

function generateRowAndCol() {
  const emptyCells = getEmptyCells();
  let generatedCell = emptyCells[getRandomInt(emptyCells.length)];

  return { row: generatedCell[0], col: generatedCell[1] };
}

function getEmptyCells() {
  let emptyCells = [];
  for (let i = 0; i < fieldSize; i++) {
    fieldContainer[i].forEach(function (elem, j) {
      if (elem === EMPTY) {
        emptyCells.push([i, j]);
      }
    });
  }

  return emptyCells;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function findWinner() {
  if (
    isRowWinner() ||
    isColWinner() ||
    isMainDiagWinner() ||
    isSideDiagWinner()
  ) {
    colorizeWinCells();
    return showMessage(`Победили ${winCell}`);
  }

  if (isDraw()) {
    return showMessage("Победила дружба!");
  }
}

function isRowWinner() {
  let winner = false;
  let i, j;

  for (i = 0; i < fieldSize; i++) {
    for (j = 1; j < fieldSize; j++) {
      if (
        fieldContainer[i][j] !== fieldContainer[i][j - 1] ||
        fieldContainer[i][j] === EMPTY
      )
        break;
    }
    if (j === fieldSize) {
      winCell = fieldContainer[i][0];
      winner = true;
      isWinner = true;
      winOrientation = "row";
      winCellNumber = i;
      break;
    }
  }

  return winner;
}

function isColWinner() {
  let winner = false;
  let i, j;

  for (i = 0; i < fieldSize; i++) {
    for (j = 1; j < fieldSize; j++)
      if (
        fieldContainer[j][i] !== fieldContainer[j - 1][i] ||
        fieldContainer[j - 1][i] === EMPTY
      )
        break;
    if (j === fieldSize) {
      winCell = fieldContainer[0][i];
      winner = true;
      isWinner = true;
      winOrientation = "col";
      winCellNumber = i;
      break;
    }
  }

  return winner;
}

function isMainDiagWinner() {
  let winner = false;
  let i, j;

  for (i = 1, j = 1; i < fieldSize; i++, j++) {
    if (
      fieldContainer[i][j] !== fieldContainer[i - 1][j - 1] ||
      fieldContainer[i][j] === EMPTY
    )
      break;
  }
  if (i === fieldSize) {
    winCell = fieldContainer[0][0];
    winner = true;
    isWinner = true;
    winOrientation = "mainDiag";
  }
  return winner;
}

function isSideDiagWinner() {
  let winner = false;
  let i, j;

  for (i = fieldSize - 2, j = 1; j < fieldSize; i--, j++) {
    if (
      fieldContainer[i][j] !== fieldContainer[i + 1][j - 1] ||
      fieldContainer[i][j] === EMPTY
    )
      break;
  }
  if (j === fieldSize) {
    winCell = fieldContainer[0][j - 1];
    winner = true;
    isWinner = true;
    winOrientation = "sideDiag";
  }

  return winner;
}

function colorizeWinCells() {
  let i, j;

  switch (winOrientation) {
    case "row":
      for (j = 0; j < fieldSize; j++) makeWinCellRed(winCellNumber, j);
      break;
    case "col":
      for (j = 0; j < fieldSize; j++) makeWinCellRed(j, winCellNumber);
      break;
    case "mainDiag":
      for (i = 0; i < fieldSize; i++) makeWinCellRed(i, i);
      break;
    case "sideDiag":
      for (i = fieldSize - 1, j = 0; j < fieldSize; i--, j++)
        makeWinCellRed(i, j);
      break;
    default:
      break;
  }
}

function makeWinCellRed(row, col) {
  let targetCell = findCell(row, col);
  targetCell.style.color = "red";
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler() {
  startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  let msg = document.querySelector(".message");
  msg.innerText = text;
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
  let container = getContainer();
  container.innerHTML = "";

  for (let i = 0; i < dimension; i++) {
    let row = document.createElement("tr");
    for (let j = 0; j < dimension; j++) {
      let cell = document.createElement("td");
      cell.textContent = EMPTY;
      cell.addEventListener("click", () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = "#333") {
  let targetCell = findCell(row, col);
  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell(row, col) {
  let container = getContainer();
  let targetRow = container.querySelectorAll("tr")[row];
  return targetRow.querySelectorAll("td")[col];
}

function getContainer() {
  return document.getElementById("fieldWrapper");
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell(row, col) {
  findCell(row, col).click();
}
